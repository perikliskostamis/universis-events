import { Component, OnInit } from '@angular/core';
import {EVENTS_CONFIGURATION} from './events.configuration';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html'
})
export class EventsComponent implements OnInit {
  public tableConfiguration: any = EVENTS_CONFIGURATION;
  public filter: any = {};
  constructor() { }

  ngOnInit() {
  }

}
