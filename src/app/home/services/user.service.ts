import {HttpErrorResponse} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {asyncMemoize} from '@universis/common';

@Injectable()
export class UserService {
  constructor(private context: AngularDataContext) {
  }

  @asyncMemoize()
  public async getInstructor() {
    try {
      const instructor = await this.context.model('instructors/me')
        .asQueryable()
        .expand('department($expand=currentYear,locale,currentPeriod,organization($expand=instituteConfiguration)),locale')
        .getItem();
      if (typeof instructor === 'undefined') {
        return Promise.reject('Instructor data cannot be found');
      }
      return Promise.resolve(instructor);
    } catch (err) {
      if (err.status === 404) {
        // throw 401 Unauthorized
        return Promise.reject(Object.assign(new HttpErrorResponse({
          error: new Error('Instructor cannot be found or is inaccessible'),
          status: 401,
        }), {
          // assign continue as for auto logout
          continue: '/auth/loginAs',
        }));
      }
      return Promise.reject(err);
    }
  }
}
