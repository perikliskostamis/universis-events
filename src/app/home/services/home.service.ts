import { Injectable } from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import * as moment from 'moment';
import {HttpClient} from '@angular/common/http';
import {UserService} from './user.service';


export declare interface IEventHoursSpecification {
  validFrom: Date;
  validThrough: Date;
  duration: moment.Duration | String;
  opens: String |any;
  closes: String |any;
  minuteOfHour: String;
  hourOfDay: String;
  dayOfMonth: String;
  monthOfYear: String;
  dayOfWeek: String;
}

export declare interface IEvent {
  about: number;
  eventHoursSpecification: IEventHoursSpecification;
  startDate: Date;
  endDate: Date;
  contributor: number;
  organizer: number;
  inLanguage: string;
  duration: moment.Duration | String;
  isAccessibleForFree: boolean;
  performer: number;
  courseClass: number;
  sections?: Array<number>;
}

@Injectable()
export class HomeService {
  constructor(private context: AngularDataContext,
              private http: HttpClient,
              private userService: UserService) {
  }

  public async createEvent() {
    const instructor = await this.userService.getInstructor();
    console.log(instructor);
    const spec: IEventHoursSpecification = {
      duration: null,
      validFrom: new Date(2021, 4, 13, 0, 0, 0, 0),
      validThrough: new Date(),
      opens: new Date(2021, 2, 3, 10, 0, 0),
      closes: new Date(2021, 2, 3, 12, 0, 0),
      minuteOfHour: '15',
      hourOfDay: '08',
      dayOfMonth: '*',
      monthOfYear: '3,4,5',
      dayOfWeek: '1,3'
    };
    spec.duration = moment.duration(moment(spec.closes).diff(moment(spec.opens))).toISOString();
    spec.opens = spec.opens.toTimeString().split(' ')[0]; // gets time while ignoring tz data
    spec.closes = spec.closes.toTimeString().split(' ')[0];
    const event: IEvent = {
      about: 600174971 as number,
      contributor: null,
      courseClass: 600174971 as number,
      duration: spec.duration,
      endDate: spec.validThrough,
      eventHoursSpecification: spec,
      inLanguage: null,
      isAccessibleForFree: true,
      organizer: 999 as number,
      performer: instructor.user as number,
      sections: [1],
      startDate: spec.validFrom
    };
    const serviceHeaders = this.context.getService().getHeaders();
    const postUrl = this.context.getService().resolve(`/instructors/me/classes/600174971/teachingEvents`);
    return this.http.post(postUrl, [event], {
      headers: serviceHeaders
    }).toPromise();
  }

  public async getEvents() {
    return this.context.model('instructors/me/teachingEvents')
      .asQueryable()
      .expand('courseClass($expand=students)')
      .getItems();
  }

  public async getClassEvents() {
    return this.context.model('instructors/me/classes/600174971/teachingEvents')
      .asQueryable()
      .expand('courseClass($expand=students)')
      .getItems();
  }
}
