export const APP_SIDEBAR_LOCATIONS = [
  {
    name: 'Home',
    key: 'Sidebar.Dashboard',
    url: '/',
    icon: 'fa fa-archive',
    index: 0
  },
  {
    name: 'Events Calendar',
    key: 'Sidebar.Events',
    url: '/eventCalendar',
    index: 10
  }
];
