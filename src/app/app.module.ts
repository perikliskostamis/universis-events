import { BrowserModule, Title } from '@angular/platform-browser';
import {NgModule, LOCALE_ID, APP_INITIALIZER, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {
  SharedModule,
  AuthModule,
  ErrorModule,
  ConfigurationService,
  APP_LOCATIONS,
  SIDEBAR_LOCATIONS,
  UserStorageService,
  LocalUserStorageService
} from '@universis/common';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { ModalModule } from 'ngx-bootstrap/modal';
import { IndexComponent } from './layouts/index.component';
import { LocationStrategy, HashLocationStrategy, registerLocaleData, CommonModule } from '@angular/common';
import { AngularDataContext, DATA_CONTEXT_CONFIG } from '@themost/angular';
import { AppSidebarModule } from '@coreui/angular';
import { environment } from '../environments/environment';
// LOCALES: import extra locales here
import en from '@angular/common/locales/en';
import { THIS_APP_LOCATIONS } from './app.locations';
import { APP_SIDEBAR_LOCATIONS } from './app.sidebar.locations';
import { EventsComponent } from './events/events.component';
import { COLUMN_FORMATTERS, TablesModule } from '@universis/tables';
import { InstructorEventsComponent } from './instuctor-events/instructor-events.component';

@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    EventsComponent,
    InstructorEventsComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    HttpClientModule,
    TranslateModule.forRoot(),
    SharedModule.forRoot(),
    RouterModule,
    AuthModule,
    FormsModule,
    AppRoutingModule,
    AppSidebarModule,
    ErrorModule.forRoot(),
    BsDropdownModule.forRoot(),
    ModalModule.forRoot(),
    TablesModule
  ],
  providers: [
    Title,
    {
        provide: DATA_CONTEXT_CONFIG, useValue: {
            base: '/',
            options: {
                useMediaTypeExtensions: false,
                useResponseConversion: true
            }
        }
    },
    TranslateService,
    {
        provide: APP_LOCATIONS,
        useValue: THIS_APP_LOCATIONS
    },
    {
      provide: SIDEBAR_LOCATIONS,
      useValue: APP_SIDEBAR_LOCATIONS
    },
    AngularDataContext,
    {
        provide: APP_INITIALIZER,
        // use APP_INITIALIZER to load application configuration
        useFactory: (configurationService: ConfigurationService) =>
            () => {
            // load application configuration
                return configurationService.load().then(() => {
                    // LOCALES: register application locales here
                    registerLocaleData(en);
                    // return true for APP_INITIALIZER
                    return Promise.resolve(true);
                });
            },
        deps: [ ConfigurationService ],
        multi: true
    },
    {
        provide: LOCALE_ID,
        useFactory: (configurationService: ConfigurationService) => {
            return configurationService.currentLocale;
        },
        deps: [ConfigurationService]
    },
    // use hash location strategy
    // https://angular.io/api/common/HashLocationStrategy
    {
        provide: LocationStrategy,
        useClass: HashLocationStrategy
    },
    {
      provide: UserStorageService,
      useClass: LocalUserStorageService
    }
  ],
  bootstrap: [AppComponent],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class AppModule {
  constructor(private _translateService: TranslateService) {
    this.ngOnInit().catch(err => {
      console.error('An error occurred while init application module.');
      console.error(err);
    });
  }
  // tslint:disable-next-line:use-life-cycle-interface
  async ngOnInit() {
    // create promises chain
    const sources = environment.languages.map(async (language) => {
      const translations = await import(`../assets/i18n/${language}.json`);
      this._translateService.setTranslation(language, translations, true);
    });
    // execute chain
    await Promise.all(sources);
  }
}
